\contentsline {chapter}{\numberline {1}Arduino Benchmark System}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Overview}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}How it Works}{1}{section.1.2}
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}
\contentsline {chapter}{\numberline {3}File Documentation}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}Arduino\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Benchmark.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}ino File Reference}{5}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{5}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Macro Definition Documentation}{6}{subsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.2.1}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}U\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}G\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}S}{6}{subsubsection.3.1.2.1}
\contentsline {subsubsection}{\numberline {3.1.2.2}N\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}U\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}M\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}E\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}S\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}S}{6}{subsubsection.3.1.2.2}
\contentsline {subsubsection}{\numberline {3.1.2.3}P\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}I}{6}{subsubsection.3.1.2.3}
\contentsline {subsection}{\numberline {3.1.3}Function Documentation}{6}{subsection.3.1.3}
\contentsline {subsubsection}{\numberline {3.1.3.1}benchmark}{6}{subsubsection.3.1.3.1}
\contentsline {subsubsection}{\numberline {3.1.3.2}loop}{6}{subsubsection.3.1.3.2}
\contentsline {subsubsection}{\numberline {3.1.3.3}setup}{6}{subsubsection.3.1.3.3}
