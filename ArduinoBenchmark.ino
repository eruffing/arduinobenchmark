/**
 * @file ArduinoBenchmark.ino
 * This file contains a benchmarking algorithm for the Arduino that calculates
 * the sine of many angles.
 *
 * @author Ethan Ruffing
 * @copyright
 *     Copyright 2014 by Ethan Ruffing. All rights reserved.
 * @since 2014-11-16
 * @version 1.1.0
 */

/**
 * @mainpage Arduino Benchmark System
 *
 * @author Ethan Ruffing
 * @copyright
 *     Copyright 2014 by Ethan Ruffing. All rights reserved.
 * @since 2014-11-16
 * @version 1.1.0
 *
 * @section sec_overview Overview
 *     This is a system designed to benchmark an Aruino UNO (or related system).
 * @section sec_how How it Works
 *     The benchmark works by generating 300 random angles and computing their
 *     sines. The time required to compute these values is measured and
 *     returned. This process is repeated for a total of 30 time measurements.
 */

/**
 * @def NUMTESTS
 *     The number of times the benchmarking process should be automatically
 *     repeated
 * @def NUMANGS
 *     The number of angles to compute the sine of during benchmarking
 * @def PI
 *     A macro to use for the mathematical constant PI
 */
#include <Arduino.h>
#include <math.h>
#define NUMTESTS 30
#define NUMANGS 300
#define PI (3.14159265359)

/**
 * Benchmarks the Arduino by generating random angles and calculating their
 * sines.
 *
 * @since 2014-11-16
 *
 * @return
 *     The time, in microseconds, that was required to perform the calculations
 */
long benchmark(float sins[]);

void setup()
{
	// Initialize serial connection
	Serial.begin(9600);

	// Seed the random number generator
	randomSeed(analogRead(0));

	// Print program information over serial
	Serial.println("BENCHMARKING SYSTEM");
	Serial.println("COPYRIGHT (C) 2014 BY ETHAN RUFFING.");
	Serial.println("BENCHMARKING WILL NOW BEGIN.");
	Serial.println("---------------------------------");
	delay(100);
}

void loop()
{
	// Declare variables
	int i, j;
	float sins[NUMANGS];
	long times[NUMTESTS];
	char c = NULL;

	// Get and display a title for this from user (for logging purposes)
	Serial.print("TEST TITLE: ");
	while (c != '\n' && c != '\r')
	{
		if (Serial.available() > 0)
		{
			c = Serial.read();
			Serial.print(c);
		}
	}
	Serial.println();
	Serial.println("---------------------------------");

	// Run benchmarks
	Serial.println("RESULTS (microseconds):");
	Serial.println("---------------------------------");
	delay(100);
	for (i = 0; i < NUMTESTS; i++)
	{
		for(j = 0; j < NUMANGS; j++)
		{
			sins[j] = 0;
		}
		Serial.println(benchmark(sins));
		delay(100);
	}
	Serial.println("---------------------------------");
	Serial.println("DONE");

	// Wait for keypress before continuing
	while(Serial.available() <= 0);
}

long benchmark(float sins[])
{
	// Declare variables
	long start, stop, elapsed;
	float a;
	int a1, a2, i;

	// Record start time
	start = micros();

	// Perform tests
	for (i = 0; i < NUMANGS; i++)
	{
		a1 = random(0, 17);
		a2 = random(1, 3);
		a = PI * (float)a1 / (float)a2;
		sins[i] = sin(a);
	}

	// Record stop time
	stop = micros();

	// Calculate elapsed time
	elapsed = stop - start;
	return elapsed;
}
