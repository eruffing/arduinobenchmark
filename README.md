Arduino Benchmarking System
===========================

Copyright 2014 by Ethan Ruffing. All rights reserved.

Overview
--------
This is a program for benchmarking an Arduino by generating 300 random angles
and calculating their sines.

This program is designed for use on the Arduino UNO family of boards. It will
run the above-described set of calculation 30 times, and display the time that
elapsed (in microseconds) during each set of calculations.

How to Use
----------
There is further information available in the API documentation (available in
several formats in the `doc` folder).

To use, simply upload the program to the Arduino UNO (or compatible device)
which you intend to benchmark. Open an interactive serial connection to the
device (it could be helpful to enable logging of the session), and follow the
prompts.

The device will run thirty tests and will output the results over the serial
connection. When complete, it will output the word "DONE".

Changelog
---------
### 1.1.0 ###
* Added ability to input title for each test
* Added random angle generation to benchmarking routine

### 1.0.0 ###
* First Release
* Basic functionality implemented
* Basic output functioning
